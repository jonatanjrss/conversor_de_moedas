# Instruções

Obtenha uma chave gratuita de api no site https://hgbrasil.com/status/finance

Crie o arquivo ``config.ini`` no mesmo diretório do main.py

Adicione as duas linhas a seguir no arquivo config.ini:

``[SECRET]``

``key = sua_chave_api`` (coloque aqui a chave de api que obteve anteriormente)

Isso é tudo.

Se quiser ver o vídeo do aplicativo em execução, segue o link: https://youtu.be/_K2BX8ejczw
