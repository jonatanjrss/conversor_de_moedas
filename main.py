from kivy.network.urlrequest import UrlRequest

from configparser import ConfigParser

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.utils import get_hex_from_color
from kivy.metrics import dp
from kivymd.theming import ThemeManager
from android.runnable import run_on_ui_thread
from jnius import autoclass


KV = """
BoxLayout   
    orientation: "vertical"
    
    MDToolbar
        title: "$ Conversor $"
        anchor_title: "center"
        md_bg_color: app.theme_cls.primary_color

    BoxLayout
        orientation: "vertical"
        padding: 20, 40
        spacing: dp(20)

        MyMDIcon
            size_hint: None, None
            icon: "currency-usd"
            font_size: dp(120)
            size: self.texture_size
            halign: "center"
            pos_hint: {"center_x": .5}

        MyMDTextFieldRound
            id: real_field
            text: app.real
            size_hint_x: 1
            hint_text: "Reais"
            icon_type: 'left'
            icon_left: 'currency-brl'
            
        MyMDTextFieldRound
            id: dollar_field
            text: app.dollar
            size_hint_x: 1
            hint_text: "Dólares"
            icon_type: 'left'
            icon_left: 'currency-usd'
            
        MyMDTextFieldRound
            id: euro_field
            text: app.euro
            size_hint_x: 1
            hint_text: "Euros"
            icon_type: 'left'
            icon_left: 'currency-eur'

        Space


<Space@Widget>

<MyMDIcon@MDIcon+Widget>
    canvas.before:
        Color:
            rgba: app.theme_cls.primary_color
        Ellipse:
            size: self.texture_size
            pos: self.pos

<MyMDTextFieldRound@MDTextFieldRound>
    #:set color_shadow [0, 0, 0, .2980392156862745]
    normal_color: color_shadow
    active_color: color_shadow
    on_text: app.currency_changed(self)
    font_size: dp(12)

"""

config = ConfigParser()
config.read('config.ini')
key = config['SECRET']['key']
url = "https://api.hgbrasil.com/finance?format=json&key={key}"

Color = autoclass("android.graphics.Color")
WindowManager = autoclass('android.view.WindowManager$LayoutParams')
activity = autoclass('org.kivy.android.PythonActivity').mActivity


class ConversorDeMoedas(App):
    
    title = "Conversor de Moedas"
    theme_cls = ThemeManager(primary_palette="Amber", theme_style = "Dark")

    real = StringProperty("")
    dollar = StringProperty("")
    euro = StringProperty("")

    def build(self):
        amber = get_hex_from_color(self.theme_cls.primary_color[:3])
        self.set_statusbar_color(amber)

        # adicionar openssl no requirements do buildozer.spec
        UrlRequest(url, self.got_data, verify=False)
        return Builder.load_string(KV)

    def on_start(self):
        self.root.ids.real_field.ids.box.height = dp(48*1.5)
        self.root.ids.dollar_field.ids.box.height = dp(48*1.5)
        self.root.ids.euro_field.ids.box.height = dp(48*1.5)

        self.root.ids.real_field.ids.icon_left.pos_hint = {"center_y": .5}
        self.root.ids.dollar_field.ids.icon_left.pos_hint = {"center_y": .5}
        self.root.ids.euro_field.ids.icon_left.pos_hint = {"center_y": .5}

    @run_on_ui_thread
    def set_statusbar_color(self, color):
        window = activity.getWindow()
        window.clearFlags(WindowManager.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.setStatusBarColor(Color.parseColor(color)) 

    def got_data(self, req, data):
        self.dollar_price = data['results']['currencies']['USD']['buy']
        self.euro_price = data['results']['currencies']['EUR']['buy']

    def currency_changed(self, instance):
        hint_text = instance.hint_text
        text = instance.text
        focused = instance._outline_color != [0,0,0,0]

        if text and focused:
            dollar_price = float(self.dollar_price)
            euro_price = float(self.euro_price)

            if hint_text == "Reais":
                real = float(text)
                self.real = format(real, ".2f")
                self.dollar = format(real / dollar_price, ".2f")
                self.euro = format(real / euro_price, ".2f")
                if not real:
                    self.clear_all()

            elif hint_text == "Dólares":
                dollar = float(text)
                self.dollar = format(dollar, ".2f")
                self.real = format(dollar * dollar_price, ".2f")                
                self.euro = format((dollar * dollar_price) / euro_price, ".2f")
                if not dollar:
                    self.clear_all()

            elif hint_text == "Euros":
                euro = float(text)
                self.euro = format(euro, ".2f")
                self.real = format(euro * euro_price, ".2f")
                self.dollar = format((euro * euro_price) / dollar_price, ".2f")
                if not euro:
                    self.clear_all()

    def clear_all(self):
        self.real = ""
        self.dollar = ""
        self.euro = ""


if __name__ == '__main__':
    ConversorDeMoedas().run()